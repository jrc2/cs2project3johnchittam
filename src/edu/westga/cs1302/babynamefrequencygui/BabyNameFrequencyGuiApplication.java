package edu.westga.cs1302.babynamefrequencygui;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

/**
 * Extends the JavaFX Application class to build the GUI and start program execution
 * 
 * @author john chittam
 * @version 1.0
 *
 */
public class BabyNameFrequencyGuiApplication extends Application {
	
	private static final String WINDOW_TITLE = "Project 3 by John Chittam";
	private static final String GUI_FXML = "view/BabyNameFrequencyGui.fxml";
	
	/**
	 * Constructs a new Application object for the program.
	 * 
	 * @precondition none
	 * @postcondition the object is ready to execute
	 */
	public BabyNameFrequencyGuiApplication() {
		super();
	}
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();
		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}
	}
	
	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}
	
	/**
	 * Launches the application.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
