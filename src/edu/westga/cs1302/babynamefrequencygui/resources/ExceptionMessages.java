package edu.westga.cs1302.babynamefrequencygui.resources;

/**
 * A class to store exception message constants
 * 
 * @author john chittam
 *
 */
public class ExceptionMessages {
	public static final String NULL_EMPTY_NAME = "name cannot be null or empty";
	public static final String INVALID_GENDER = "gender cannot be null or empty and must equal MALE or FEMALE";
	public static final String INVALID_BIRTH_YEAR = "birth year must be >= 1880 and <= current year";
	public static final String NULL_NAME_KEY = "name key cannot be null";
	public static final String INVALID_FREQUENCY = "frequency must be >= 1";
	public static final String NULL_BABY_NAME_OBJECT = "baby name object cannot be null";
	public static final String COLLECTION_CONTAINS_NULL = "collection contains a null object";
}
