package edu.westga.cs1302.babynamefrequencygui.viewmodel;

import java.util.ArrayList;
import java.util.Collections;

import edu.westga.cs1302.babynamefrequencygui.model.BabyMap;
import edu.westga.cs1302.babynamefrequencygui.model.BabyName;
import edu.westga.cs1302.babynamefrequencygui.resources.ExceptionMessages;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

/**
 * Controls the chart interaction login and generates chart data
 * 
 * @author john chittam
 *
 */
public class ChartInteraction {
	
	private StringProperty barChartTitleProperty;
	private ObservableList<String> namesList;
	private ObjectProperty<ObservableList<XYChart.Series<String, Number>>> barChartDataProperty;
	
	private ObjectProperty<ObservableList<XYChart.Series<Number, Number>>> lineChartDataProperty;
	private StringProperty lineChartTitleProperty;
	private StringProperty lineChartXAxisLabelProperty;
	private IntegerProperty lineChartLowerBound;
	private IntegerProperty lineChartUpperBound;
	private StringProperty lineChartYAxisLabelProperty;
	
	private BabyNameListProcessor listProcessor = new BabyNameListProcessor();
	
	/**
	 * Instantiates new ChartInteraction
	 * 
	 * @precondition none
	 * @postcondition variables instantiated
	 */
	public ChartInteraction() {
		this.barChartTitleProperty = new SimpleStringProperty("");
		this.namesList = FXCollections.observableArrayList();
		this.barChartDataProperty = new SimpleObjectProperty<ObservableList<XYChart.Series<String, Number>>>(
				FXCollections.observableArrayList());
		
		this.lineChartDataProperty = new SimpleObjectProperty<ObservableList<XYChart.Series<Number, Number>>>(
				FXCollections.observableArrayList());
		this.lineChartTitleProperty = new SimpleStringProperty();
		this.lineChartXAxisLabelProperty = new SimpleStringProperty();
		this.lineChartYAxisLabelProperty = new SimpleStringProperty();
		this.lineChartLowerBound = new SimpleIntegerProperty();
		this.lineChartUpperBound = new SimpleIntegerProperty();
	}
	
	/**
	 * Gets the barChartTitleProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the barChartTitleProperty
	 */
	public StringProperty barChartTitleProperty() {
		return this.barChartTitleProperty;
	}
	
	/**
	 * Gets the barChartDataProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the barChartDataProperty
	 */
	public ObjectProperty<ObservableList<XYChart.Series<String, Number>>> barChartDataProperty() {
		return this.barChartDataProperty;
	}
	
	/**
	 * Gets the namesList
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the namesList
	 */
	public ObservableList<String> namesList() {
		return this.namesList;
	}
	
	/**
	 * Gets the lineChartDataProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the lineChartDataProperty
	 */
	public ObjectProperty<ObservableList<XYChart.Series<Number, Number>>> lineChartDataProperty() {
		return this.lineChartDataProperty;
	}
	
	/**
	 * Gets the lineChartTitleProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the lineChartTitleProperty
	 */
	public StringProperty lineChartTitleProperty() {
		return this.lineChartTitleProperty;
	}
	
	/**
	 * Gets the lineChartXAxisLabelProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the lineChartXAxisLabelProperty
	 */
	public StringProperty lineChartXAxisLabelProperty() {
		return this.lineChartXAxisLabelProperty;
	}
	
	/**
	 * Gets the lineChartYAxisLabelProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the lineChartYAxisLabelProperty
	 */
	public StringProperty lineChartYAxisLabelProperty() {
		return this.lineChartYAxisLabelProperty;
	}
	
	/**
	 * Gets the lineChartLowerBound
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the lineChartLowerBound
	 */
	public IntegerProperty lineChartLowerBound() {
		return this.lineChartLowerBound;
	}
	
	/**
	 * Gets the lineChartUpperBound
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the lineChartUpperBound
	 */
	public IntegerProperty lineChartUpperBound() {
		return this.lineChartUpperBound;
	}
	
	/**
	 * Generates bar chart data for the given year
	 * 
	 * @precondition none
	 * @postcondition data generated
	 * @param year the year for which to generate the chart
	 * @param babyMap the map of BabyName objects
	 */
	public void generateBarChartData(int year, BabyMap babyMap) {
		this.barChartTitleProperty.set("Most Popular Names in " + year);
		this.namesList.clear();
		ArrayList<BabyName> topFemale = this.getTopNames(year, "FEMALE", babyMap);
		ArrayList<BabyName> topMale = this.getTopNames(year, "MALE", babyMap);
		
		XYChart.Series<String, Number> female = new XYChart.Series<String, Number>();
		female.setName("Female");
		for (BabyName currFemale : topFemale) {
			this.namesList.add(currFemale.getNameKey().getName());
			female.getData().add(new XYChart.Data<String, Number>(currFemale.getNameKey().getName(), currFemale.getFrequency()));
		}
		
		XYChart.Series<String, Number> male = new XYChart.Series<String, Number>();
		male.setName("Male");
		for (BabyName currMale : topMale) {
			this.namesList.add(currMale.getNameKey().getName());
			male.getData().add(new XYChart.Data<String, Number>(currMale.getNameKey().getName(), currMale.getFrequency()));
		}
		
		if (topFemale.isEmpty() && topMale.isEmpty()) {
			throw new IllegalArgumentException("No names exist for this year");
		}
		
		this.barChartDataProperty.get().clear();
		this.barChartDataProperty.get().add(female);
		this.barChartDataProperty.get().add(male);
	}
	
	private ArrayList<BabyName> getTopNames(int year, String gender, BabyMap babyMap) {
		ArrayList<BabyName> matchingBabyNames = new ArrayList<BabyName>();
		ArrayList<BabyName> topThree = new ArrayList<BabyName>();
		for (BabyName curr : babyMap) {
			if (curr.getNameKey().getBirthYear() == year && curr.getNameKey().getGender().equalsIgnoreCase(gender)) {
				matchingBabyNames.add(curr);
			}
		}
		Collections.sort(matchingBabyNames, this.listProcessor.getSortedByGenderComparator());
		if (matchingBabyNames.size() >= 1) {
			topThree.add(matchingBabyNames.get(0));
		}
		if (matchingBabyNames.size() >= 2) {
			topThree.add(matchingBabyNames.get(1));
		}
		if (matchingBabyNames.size() >= 3) {
			topThree.add(matchingBabyNames.get(2));
		}
		
		return topThree;
	}
	
	/**
	 * Generates a line chart showing the frequency a given name was given to a gender
	 * 
	 * @precondition name!=null AND !name.isEmpty() AND gender.equals("MALE"|"FEMALE")
	 * 				 AND babyMap!=null AND !babyMap.isEmpty()
	 * @postcondition none
	 * 
	 * @param name the name to check
	 * @param gender the gender to check
	 * @param babyMap the map containing BabyName objects to check through
	 * @return true if graph made, false if not
	 */
	public boolean generateLineChartData(String name, String gender, BabyMap babyMap) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_EMPTY_NAME);
		}
		if ((!gender.equalsIgnoreCase("MALE")) && (!gender.equalsIgnoreCase("FEMALE"))) {
			throw new IllegalArgumentException(ExceptionMessages.INVALID_GENDER);
		}
		if (babyMap == null || babyMap.isEmpty()) {
			System.err.println("babyMap is null or empty");
			return false;
		}
		
		this.setupLineChartDisplay(name, gender);
		ArrayList<BabyName> matches = this.getLineChartMatches(name, gender, babyMap);
		
		int firstYear;
		int lastYear;
		
		try {
			firstYear = matches.get(0).getNameKey().getBirthYear();
			lastYear = matches.get(matches.size() - 1).getNameKey().getBirthYear();
		} catch (IndexOutOfBoundsException ex) {
			return false;
		}
		
		XYChart.Series<Number, Number> series = new XYChart.Series<Number, Number>();
		for (BabyName curr : matches) {
			series.getData().add(new XYChart.Data<Number, Number>(curr.getNameKey().getBirthYear(), curr.getFrequency()));
		}
		
		return this.setupLineChart(series, firstYear, lastYear);
	}
	
	private void setupLineChartDisplay(String name, String gender) {
		this.lineChartTitleProperty.set("Frequencies for " + name + " (" + gender.toUpperCase() + ")");
		this.lineChartXAxisLabelProperty.set("Year");
		this.lineChartYAxisLabelProperty.set("Frequency");
	}
	
	private ArrayList<BabyName> getLineChartMatches(String name, String gender, BabyMap babyMap) {
		ArrayList<BabyName> matches = new ArrayList<BabyName>();
		for (BabyName curr : babyMap) {
			if (curr.getNameKey().getName().equalsIgnoreCase(name) && curr.getNameKey().getGender().equalsIgnoreCase(gender)) {
				matches.add(curr);
			}
		}
		Collections.sort(matches, this.listProcessor.getSortedByNameComparator());
		
		return matches;
	}

	private boolean setupLineChart(XYChart.Series<Number, Number> series, int firstYear, int lastYear) {
		try {
			this.lineChartDataProperty.get().clear();
			this.lineChartDataProperty.get().add(series);
			this.lineChartLowerBound.set(firstYear);
			this.lineChartUpperBound.set(lastYear);
		} catch (IllegalArgumentException | NullPointerException ex) {
			System.err.println(ex.getMessage());
			return false;
		}
		
		return true;
	}
}
