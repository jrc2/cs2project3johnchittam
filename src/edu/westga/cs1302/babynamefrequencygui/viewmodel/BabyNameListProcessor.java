package edu.westga.cs1302.babynamefrequencygui.viewmodel;

import java.util.Comparator;

import edu.westga.cs1302.babynamefrequencygui.model.BabyName;

/**
 * This class provides methods for processing a list of BabyName objects
 * 
 * @author john chittam
 *
 */
public class BabyNameListProcessor {

	/**
	 * Returns a comparator to sort a collection of BabyName by name,
	 * then gender, then year
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a comparator to sort a collection of BabyName by name,
	 * 		   then gender, then year
	 */
	public Comparator<BabyName> getSortedByNameComparator() {
		Comparator<BabyName> comparator = new Comparator<BabyName>() {
			@Override
			public int compare(BabyName name1, BabyName name2) {
				if (name1.getNameKey().getName().compareTo(name2.getNameKey().getName()) != 0) {
					return name1.getNameKey().getName().compareTo(name2.getNameKey().getName());
				} else if (name1.getNameKey().getGender().compareTo(name2.getNameKey().getGender()) != 0) {
					return name1.getNameKey().getGender().compareTo(name2.getNameKey().getGender());
				} else {
					if (name1.getNameKey().getBirthYear() > name2.getNameKey().getBirthYear()) {
						return 1;
					} else if (name1.getNameKey().getBirthYear() < name2.getNameKey().getBirthYear()) {
						return -1;
					} else {
						return 0;
					}
				}
			}
		};
		return comparator;
	}
	
	/**
	 * Returns a comparator to sort a collection of BabyName by gender,
	 * then year, then descending frequency
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a comparator to sort a collection of BabyName by gender,
	 * 		   then year, then descending frequency
	 */
	public Comparator<BabyName> getSortedByGenderComparator() {
		Comparator<BabyName> comparator = new Comparator<BabyName>() {
			@Override
			public int compare(BabyName name1, BabyName name2) {
				if (name1.getNameKey().getGender().compareTo(name2.getNameKey().getGender()) != 0) {
					return name1.getNameKey().getGender().compareTo(name2.getNameKey().getGender());
				} else {
					if (name1.getNameKey().getBirthYear() > name2.getNameKey().getBirthYear()) {
						return 1;
					} else if (name1.getNameKey().getBirthYear() < name2.getNameKey().getBirthYear()) {
						return -1;
					} else {
						if (name1.getFrequency() < name2.getFrequency()) {
							return 1;
						} else if (name1.getFrequency() > name2.getFrequency()) {
							return -1;
						} else {
							return 0;
						}
					}
				}
			}
		};
		return comparator;
	}
	
}
