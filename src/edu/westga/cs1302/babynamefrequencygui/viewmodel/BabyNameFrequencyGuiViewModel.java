package edu.westga.cs1302.babynamefrequencygui.viewmodel;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import edu.westga.cs1302.babynamefrequencygui.model.BabyMap;
import edu.westga.cs1302.babynamefrequencygui.model.BabyName;
import edu.westga.cs1302.babynamefrequencygui.model.BabyNameKey;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The view model class
 * 
 * @author john chittam
 *
 */
public class BabyNameFrequencyGuiViewModel {
	private final StringProperty stateProperty;
	private final StringProperty errorProperty;
	private final ListProperty<BabyName> namesProperty;
	private final StringProperty nameProperty;
	private final StringProperty yearProperty;
	private final StringProperty genderProperty;
	private final StringProperty frequencyProperty;
	
	private BabyMap babyMap;
	private BabyNameListProcessor listProcessor = new BabyNameListProcessor();
	private static final String COMMA = ",";
	
	/**
	 * Instantiates a new view model
	 * 
	 * @precondition none
	 * @postcondition stateProperty()!=null && namesProperty()!=null &&
	 * 				  nameProperty()!=null && yearProperty()!=null &&
	 * 				  genderProperty()!=null && grequencyProperty()!=null &&
	 * 
	 */
	public BabyNameFrequencyGuiViewModel() {
		this.errorProperty = new SimpleStringProperty();
		this.stateProperty = new SimpleStringProperty();
		
		this.babyMap = new BabyMap();
		this.namesProperty = new SimpleListProperty<BabyName>(FXCollections.observableArrayList(this.babyMap));
		
		this.nameProperty = new SimpleStringProperty();
		this.yearProperty = new SimpleStringProperty();
		this.genderProperty = new SimpleStringProperty();
		this.frequencyProperty = new SimpleStringProperty("");
	}
	
	/**
	 * Gets the errorProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the errorProperty
	 */
	public StringProperty errorProperty() {
		return this.errorProperty;
	}

	/**
	 * Gets the stateProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the stateProperty
	 */
	public StringProperty stateProperty() {
		return this.stateProperty;
	}
	
	/**
	 * Gets the babyMap
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the babyMap
	 */
	public BabyMap babyMap() {
		return this.babyMap;
	}

	/**
	 * Gets the namesProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the namesProperty
	 */
	public ListProperty<BabyName> namesProperty() {
		return this.namesProperty;
	}

	/**
	 * Gets the nameProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the nameProperty
	 */
	public StringProperty nameProperty() {
		return this.nameProperty;
	}

	/**
	 * Gets the yearProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the yearProperty
	 */
	public StringProperty yearProperty() {
		return this.yearProperty;
	}

	/**
	 * Gets the genderProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the genderProperty
	 */
	public StringProperty genderProperty() {
		return this.genderProperty;
	}

	/**
	 * Gets the frequencyProperty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the frequencyProperty
	 */
	public StringProperty frequencyProperty() {
		return this.frequencyProperty;
	}
	
	/**
	 * Adds the baby name
	 * 
	 * @precondition BabyName with key cannot already exist
	 * @postcondition none
	 * 
	 * @return true if name added, false if not
	 */
	public boolean addBabyName() {
		String name = this.nameProperty.get();
		int year = Integer.parseInt(this.yearProperty.get());
		String gender = this.genderProperty.get().toUpperCase();
		int frequency = Integer.parseInt(this.frequencyProperty.get());
		BabyNameKey key = new BabyNameKey(name, gender, year);
		
		if (this.babyMap.getBabies().containsKey(key)) {
			throw new IllegalArgumentException("This name, year, and gender combo already exists.");
		}
		
		BabyName babyName = null;
		babyName = new BabyName(key, frequency);
		
		if (this.babyMap.add(babyName)) {
			this.clear();
			this.reloadListView();
			return true;
		}
		
		return false;
	}
	
	/**
	 * Adds BabyName objects from comma separated file
	 * 
	 * @precondition file!=null
	 * @postcondition all valid objects added
	 * 
	 * @param file the file to process
	 * @return true if at least one name added, false otherwise
	 */
	public boolean addFromFile(File file) {
		BabyMap newBabyNames = new BabyMap();
		String state = "N/A";
		ArrayList<String> lines = this.splitFile(file);
		
		for (String currLine : lines) {
			String[] newNameArray = currLine.split(",");
			try {
				state = newNameArray[0];
				String gender = "NONE";
				if (newNameArray[1].equals("F")) {
					gender = "FEMALE";
				} else if (newNameArray[1].equals("M")) {
					gender = "MALE";
				}
				BabyName newName = new BabyName(new BabyNameKey(newNameArray[3], gender, Integer.parseInt(newNameArray[2])), Integer.parseInt(newNameArray[4]));
				newBabyNames.add(newName);
			} catch (IllegalArgumentException | NullPointerException ex) {
				System.err.println("Could not add name: " + currLine);
			}
		}
		
		if (newBabyNames.size() >= 1) {
			this.babyMap.removeAll(this.babyMap.getBabies().values());
			this.babyMap.addAll(newBabyNames.getBabies().values());
			this.stateProperty.set(state);
			this.reloadListView();
			return true;
		}
		return false;
	}
	
	private ArrayList<String> splitFile(File file) {
		ArrayList<String> lines = new ArrayList<String>();
		try (Scanner scan = new Scanner(file)) {
			while (scan.hasNextLine()) {
				lines.add(scan.nextLine());
			}
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
		
		return lines;
	}
	
	/**
	 * Saves name date to comma separated file
	 * 
	 * @precondition file!=null
	 * @postcondition file written
	 * 
	 * @param file file to write to
	 * @return true if successful, false if not
	 */
	public boolean saveToFile(File file) {
		if (file == null) {
			throw new IllegalArgumentException("file cannot be null");
		}
		boolean fileWritten = false;
		try (PrintWriter writer = new PrintWriter(file)) {
			for (BabyName curr : this.babyMap) {
				String state = this.stateProperty.get();
				char gender = curr.getNameKey().getGender().charAt(0);
				int year = curr.getNameKey().getBirthYear();
				String name = curr.getNameKey().getName();
				int frequency = curr.getFrequency();
				writer.println(state + COMMA + gender + COMMA + year + COMMA + name + COMMA + frequency);
				fileWritten = true;
			}
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
		
		return fileWritten;
	}

	/**
	 * Removes a babyName object
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true if object removed, false otherwise
	 */
	public boolean removeBabyName() {
		String name = this.nameProperty.get();
		int year = Integer.parseInt(this.yearProperty.get());
		String gender = this.genderProperty.get().toUpperCase();
		BabyNameKey key = new BabyNameKey(name, gender, year);

		if (this.babyMap.removeByKey(key)) {
			this.clear();
			this.reloadListView();
			return true;
		}
		
		return false;
	}
	
	/**
	 * Searches for a baby name frequency given name, year, and gender
	 * 
	 * @precondition namesProperty.size() >= 1 AND !nameProperty.get().isEmpty() AND !yearProperty.get().isEmpty() 
	 * 				 AND !genderProperty.get().isEmpty() AND frequencyProperty.get().equals("")
	 * @postcondition frequency is found and namesProperty only has the found BabyName
	 * 
	 * @return true is search successful, false is not
	 */
	public boolean searchForFrequency() {
		if (this.namesProperty.size() < 1) {
			throw new IllegalArgumentException("No BabyNames have been added yet");
		}
		if (this.nameProperty.get().isEmpty() || this.yearProperty.get().isEmpty() || this.genderProperty.get().isEmpty()) {
			throw new IllegalArgumentException("Name, year, and gender must be given to search");
		}
		if (!this.frequencyProperty.get().isEmpty()) {
			throw new IllegalArgumentException("Frequency must be blank to search");
		}
		
		String name = this.nameProperty.get();
		int year = Integer.parseInt(this.yearProperty.get());
		String gender = this.genderProperty.get().toUpperCase();
		BabyNameKey key = new BabyNameKey(name, gender, year);
		
		BabyName babyName = this.babyMap.getBabies().get(key);
		
		if (babyName != null) {
			this.frequencyProperty.set("" + babyName.getFrequency());
			this.displayOneBabyName(babyName);
			return true;
		}
		return false;
		
	}
	
	private void displayOneBabyName(BabyName babyName) {
		BabyMap babyMap = new BabyMap();
		babyMap.add(babyName);
		this.namesProperty.set(FXCollections.observableArrayList(babyMap));
	}
	
	/**
	 * Updates the frequency for an existing BabyName
	 * 
	 * @precondition BabyName with key must exist
	 * @postcondition frequency updated
	 * 
	 * @return true is frequency changed, false otherwise
	 */
	
	public boolean updateFrequency() {
		String name = this.nameProperty.get();
		int year = Integer.parseInt(this.yearProperty.get());
		String gender = this.genderProperty.get().toUpperCase();
		int frequency = Integer.parseInt(this.frequencyProperty.get());
		BabyNameKey key = new BabyNameKey(name, gender, year);
		
		if (!this.babyMap.getBabies().containsKey(key)) {
			throw new IllegalArgumentException("This name, year, and gender combo does not exist");
		}
		
		BabyName babyName = null;
		babyName = new BabyName(key, frequency);
		
		if (this.babyMap.add(babyName)) {
			this.clear();
			this.reloadListView();
			return true;
		}
		
		return false;
	}
	
	/**
	 * Sorts by name, then gender, then year
	 * 
	 * @precondition none
	 * @postcondition namesProperty shows sorted list
	 */
	public void sortByName() {
		ArrayList<BabyName> babyNames = new ArrayList<BabyName>();
		babyNames.addAll(this.babyMap);
		Collections.sort(babyNames, this.listProcessor.getSortedByNameComparator());
		this.namesProperty.set(FXCollections.observableArrayList(babyNames));
	}
	
	/**
	 * Sorts by gender, then year, then descending frequency
	 * 
	 * @precondition none
	 * @postcondition namesProperty shows sorted list
	 */
	public void sortByGender() {
		ArrayList<BabyName> babyNames = new ArrayList<BabyName>();
		babyNames.addAll(this.babyMap);
		Collections.sort(babyNames, this.listProcessor.getSortedByGenderComparator());
		this.namesProperty.set(FXCollections.observableArrayList(babyNames));
	}
	
	private void clear() {
		this.nameProperty.set("");
		this.yearProperty.set("");
		this.genderProperty.set("");
		this.frequencyProperty.set("");
		this.errorProperty.set("");
	}
	
	/**
	 * Loads all names to namesProperty using default sorting
	 * 
	 * @precondition none
	 * @postcondition all names in namesProperty and sorted by default sorting
	 */
	public void reloadListView() {
		ArrayList<BabyName> babyNames = new ArrayList<BabyName>();
		babyNames.addAll(this.babyMap);
		Collections.sort(babyNames);
		this.namesProperty.set(FXCollections.observableArrayList(babyNames));
	}
}
