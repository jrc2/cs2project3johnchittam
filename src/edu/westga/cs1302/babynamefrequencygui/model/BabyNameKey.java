package edu.westga.cs1302.babynamefrequencygui.model;

import java.time.LocalDate;

import edu.westga.cs1302.babynamefrequencygui.resources.ExceptionMessages;

/**
 * The Baby Name Key class.
 *
 * @author john chittam
 * 
 */
public class BabyNameKey {
	
	private String name;
	private String gender;
	private int birthYear;
	
	private static final int CURRENT_YEAR = LocalDate.now().getYear();
	private static final String SEPARATOR = "_";
	
	/**
	 * The baby name key constructor.
	 *
	 * @param name the baby's name
	 * @param gender the baby's gender
	 * @param birthYear the baby's birth year
	 * @precondition name!=null AND !name.trim().isEmpty() AND (gender.trim().equalsIgnoreCase("male")
	 * 				 || gender.trim().equalsIgnoreCase("female")) AND 1775<=birthYear<=CURRENT_YEAR
	 * @postcondition this.getName().equals(name) AND this.getGender().equals(gender)
	 * 				  AND this.getBirthYear==birthYear
	 */
	public BabyNameKey(String name, String gender, int birthYear) {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_EMPTY_NAME);
		}
		if ((!gender.equalsIgnoreCase("male")) && (!gender.equalsIgnoreCase("female"))) {
			throw new IllegalArgumentException(ExceptionMessages.INVALID_GENDER);
		}
		if (birthYear < 1775 || birthYear > CURRENT_YEAR) {
			throw new IllegalArgumentException(ExceptionMessages.INVALID_BIRTH_YEAR);
		}
		
		this.name = name.trim();
		this.gender = gender.trim().toUpperCase();
		this.birthYear = birthYear;
	}

	
	/**
	 * Gets the name.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the gender.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the gender
	 */
	public String getGender() {
		return this.gender;
	}

	/**
	 * Gets the birth year.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the birth year
	 */
	public int getBirthYear() {
		return this.birthYear;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + birthYear;
		result = prime * result + ((this.gender == null) ? 0 : this.gender.hashCode());
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object babyName) {
		if (this == babyName) {
			return true;
		}
		if (babyName == null) {
			return false;
		}
		if (getClass() != babyName.getClass()) {
			return false;
		}
		BabyNameKey otherBabyName = (BabyNameKey) babyName;
		if (this.birthYear != otherBabyName.birthYear) {
			return false;
		}
		if (this.gender == null) {
			if (otherBabyName.gender != null) {
				return false;
			}
		} else if (!this.gender.equals(otherBabyName.gender)) {
			return false;
		}
		if (this.name == null) {
			if (otherBabyName.name != null) {
				return false;
			}
		} else if (!this.name.equals(otherBabyName.name)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return this.name + SEPARATOR + this.gender + SEPARATOR + this.birthYear;
	}
	
}
