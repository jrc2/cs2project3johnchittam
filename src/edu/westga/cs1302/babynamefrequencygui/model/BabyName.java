package edu.westga.cs1302.babynamefrequencygui.model;

import edu.westga.cs1302.babynamefrequencygui.resources.ExceptionMessages;

/**
 * The Baby Name class
 * 
 * @author john chittam
 *
 */
public class BabyName implements Comparable<BabyName> {
	private BabyNameKey nameKey;
	private int frequency;
	private static final String COMMA_SPACE = ", ";
	private static final String COLON_SPACE = ": ";
	
	/**
	 * Instantiates a new baby name.
	 * 
	 * @precondition nameKey!=null AND frequency>0
	 * @postcondition this.getNameKey()==nameKey AND this.getFrequency()==frequency
	 *
	 * @param nameKey the name key
	 * @param frequency the frequency
	 */
	public BabyName(BabyNameKey nameKey, int frequency) {
		if (nameKey == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_NAME_KEY);
		}
		if (frequency <= 0) {
			throw new IllegalArgumentException(ExceptionMessages.INVALID_FREQUENCY);
		}
		
		this.nameKey = nameKey;
		this.frequency = frequency;
	}

	/**
	 * Gets the name key.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name key
	 */
	public BabyNameKey getNameKey() {
		return this.nameKey;
	}

	/**
	 * Gets the frequency.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the frequency
	 */
	public int getFrequency() {
		return this.frequency;
	}
	
	@Override
	public String toString() {
		String name = this.getNameKey().getName();
		String genderLetter = "";
		if (this.getNameKey().getGender().equalsIgnoreCase("MALE")) {
			genderLetter = "M";
		} else if (this.getNameKey().getGender().equalsIgnoreCase("FEMALE")) {
			genderLetter = "F";
		}
		int year = this.getNameKey().getBirthYear();
		
		return name + COMMA_SPACE + genderLetter + COMMA_SPACE + year + COLON_SPACE + this.frequency;
	}

	@Override
	public int compareTo(BabyName otherName) {
		if (this.getNameKey().getBirthYear() > otherName.getNameKey().getBirthYear()) {
			return 1;
		} else if (this.getNameKey().getBirthYear() < otherName.getNameKey().getBirthYear()) {
			return -1;
		} else {
			if (this.getNameKey().getName().compareTo(otherName.getNameKey().getName()) != 0) {
				return this.getNameKey().getName().compareTo(otherName.getNameKey().getName());
			} else {
				return this.getNameKey().getGender().compareTo(otherName.getNameKey().getGender());
			}
		}
	}
	
}
