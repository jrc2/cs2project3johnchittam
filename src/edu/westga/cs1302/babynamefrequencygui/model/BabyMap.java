package edu.westga.cs1302.babynamefrequencygui.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import edu.westga.cs1302.babynamefrequencygui.resources.ExceptionMessages;

/**
 * The Baby Map class
 * 
 * @author john chittam
 *
 */
public class BabyMap implements Collection<BabyName> {
	private final Map<BabyNameKey, BabyName> babies;
	
	/**
	 * Instantiates a new baby map.
	 * 
	 * @precondition none
	 * @postcondition this.babies instantiated
	 */
	public BabyMap() {
		this.babies = new HashMap<BabyNameKey, BabyName>();
	}
	
	/**
	 * Gets the babies map
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the babies map
	 */
	public Map<BabyNameKey, BabyName> getBabies() {
		return this.babies;
	}
	
	/**
	 * Removes BabyName object by key
	 * 
	 * @precondition key != null
	 * @postcondition this.babies.containsKey(key)==false
	 * 
	 * @param key the key to remove
	 * @return true if object with key removed, false if not
	 */
	public boolean removeByKey(BabyNameKey key) {
		if (key == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_NAME_KEY);
		}

		if (this.babies.containsKey(key)) {
			this.babies.remove(key);
			return true;
		}
		
		return false;
	}

	@Override
	public boolean add(BabyName babyName) {
		if (babyName == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_BABY_NAME_OBJECT);
		}
		
		this.babies.put(babyName.getNameKey(), babyName);
		return this.babies.containsKey(babyName.getNameKey());
	}

	@Override
	public boolean addAll(Collection<? extends BabyName> babyNames) {
		for (BabyName curr : babyNames) {
			if (curr == null) {
				throw new NullPointerException(ExceptionMessages.COLLECTION_CONTAINS_NULL);
			}
			this.add(curr);
		}
		
		return true;
	}

	@Override
	public void clear() {
		this.babies.clear();
		
	}

	@Override
	public boolean contains(Object babyName) {
		if (babyName == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_BABY_NAME_OBJECT);
		}
		
		return this.babies.containsValue(babyName);
	}

	@Override
	public boolean containsAll(Collection<?> babyNames) {
		for (Object curr : babyNames) {
			if (curr == null) {
				throw new NullPointerException(ExceptionMessages.COLLECTION_CONTAINS_NULL);
			}
			
			if (!this.contains(curr)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public boolean isEmpty() {
		return this.babies.isEmpty();
	}

	@Override
	public Iterator<BabyName> iterator() {
		return this.babies.values().iterator();
	}

	@Override
	public boolean remove(Object babyNameObject) {
		if (babyNameObject == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_BABY_NAME_OBJECT);
		}
		
		this.babies.remove(babyNameObject);
		return this.babies.containsValue(babyNameObject);
	}

	@Override
	public boolean removeAll(Collection<?> babyNames) {
		boolean removedAll = true;
		
		for (Object curr : babyNames) {
			if (curr == null) {
				throw new NullPointerException(ExceptionMessages.COLLECTION_CONTAINS_NULL);
			}
			
			if (!this.remove(curr)) {
				removedAll = false;
			}
		}
		
		return removedAll;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int size() {
		return this.babies.size();
	}

	@Override
	public Object[] toArray() {
		return this.babies.values().toArray();
	}

	@Override
	public <T> T[] toArray(T[] babyNames) {
		return this.babies.values().toArray(babyNames);
	}
}
