package edu.westga.cs1302.babynamefrequencygui.model.test.babyname;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.babynamefrequencygui.model.BabyName;
import edu.westga.cs1302.babynamefrequencygui.model.BabyNameKey;

class TestConstructor {

	@Test
	void testEverythingValid() {
		BabyNameKey key = new BabyNameKey("John", "male", 2000);
		BabyName name = new BabyName(key, 22);
		assertAll(
			()-> assertEquals("John", name.getNameKey().getName()),
			()-> assertEquals("MALE", name.getNameKey().getGender()),
			()-> assertEquals(2000, name.getNameKey().getBirthYear()),
			()-> assertEquals(22, name.getFrequency())
		);
	}
	
	@Test
	void testNullNameKey() {
		assertThrows(IllegalArgumentException.class, 
			()-> new BabyName(null, 2)
		);
	}
	
	@Test
	void testZeroFrequency() {
		BabyNameKey key = new BabyNameKey("John", "male", 2000);
		
		assertThrows(IllegalArgumentException.class, 
			()-> new BabyName(key, 0)
		);
	}
	
	@Test
	void testNegativeFrequency() {
		BabyNameKey key = new BabyNameKey("John", "male", 2000);
		
		assertThrows(IllegalArgumentException.class, 
			()-> new BabyName(key, -1)
		);
	}

}
