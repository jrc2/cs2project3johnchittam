package edu.westga.cs1302.babynamefrequencygui.view;

import java.io.File;
import java.time.LocalDate;

import edu.westga.cs1302.babynamefrequencygui.model.BabyName;
import edu.westga.cs1302.babynamefrequencygui.viewmodel.BabyNameFrequencyGuiViewModel;
import edu.westga.cs1302.babynamefrequencygui.viewmodel.ChartInteraction;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

/**
 * The code behind class
 * 
 * @author john chittam
 *
 */
public class BabyNameFrequencyGuiCodeBehind {

	private BabyNameFrequencyGuiViewModel viewmodel;
	private ChartInteraction chartInteraction;
	
	@FXML
	private AnchorPane guiPane;

	@FXML
	private MenuItem fileMenuOpenButton;

	@FXML
	private MenuItem fileMenuSaveButton;

	@FXML
	private MenuItem sortMenuByNameButton;

	@FXML
	private MenuItem sortMenuByGenderButton;

	@FXML
	private Label currentStateLabel;

	@FXML
	private Label errorMessageField;

	@FXML
	private ListView<BabyName> namesListView;

	@FXML
    private TextField nameField;

    @FXML
    private Label nameFormatLabel;

    @FXML
    private TextField yearField;

    @FXML
    private Label yearFormatLabel;

	@FXML
	private ComboBox<String> genderComboBox;

	@FXML
	private TextField frequencyField;
	
	@FXML
    private Label frequencyFormatLabel;

	@FXML
	private Button addButton;

	@FXML
	private Button deleteButton;

	@FXML
	private Button searchButton;

	@FXML
	private Button updateFrequencyButton;

	@FXML
	private Button reloadButton;
	
	@FXML
    private Pane barChartPane;

	@FXML
	private Button barChartPrevYearButton;
	
	@FXML
    private TextField barChartYearField;
	
	@FXML
    private Label barGraphYearFormatLabel;

	@FXML
	private Button barGraphNextYearButton;
	
	@FXML
    private Pane lineChartPane;

	@FXML
	private TextField lineGraphNameField;
	
	@FXML
    private Label lineChartNameFormatLabel;

	@FXML
	private ComboBox<String> lineGraphGenderComboBox;

	@FXML
	private Button lineGraphSubmitButton;
	
	private SimpleStringProperty genderChoice;
	private BarChart<String, Number> barChart;
	private LineChart<Number, Number> lineChart;
	private SimpleIntegerProperty lineChartLowerBound;
	private SimpleIntegerProperty lineChartUpperBound;
	private SimpleStringProperty lineChartGenderChoice;
	
	/**
	 * Instantiates a new code behind
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public BabyNameFrequencyGuiCodeBehind() {
		this.viewmodel = new BabyNameFrequencyGuiViewModel();
		this.chartInteraction = new ChartInteraction();
		this.genderChoice = new SimpleStringProperty("");
		this.lineChartLowerBound = new SimpleIntegerProperty();
		this.lineChartUpperBound = new SimpleIntegerProperty();
		this.lineChartGenderChoice = new SimpleStringProperty("");
	}

	@FXML
	void initialize() {
		this.errorMessageField.textProperty().set("");
		this.setupComboboxes();
		this.setupBindings();
		this.setupChangeListeners();
		this.setupBooleanBindings();
		this.setupBarChart();
		this.setupLineChart();
	}

	private void setupBarChart() {
		this.barChart = new BarChart<String, Number>(new CategoryAxis(), new NumberAxis());	
		this.barChart.setAnimated(false);
		this.barChartPane.getChildren().add(this.barChart);
		this.barChart.setVisible(false);
		this.barChart.getXAxis().labelProperty().set("");
		this.barChart.getYAxis().labelProperty().set("");
	}
	
	private void setupLineChart() {
		this.lineChart = new LineChart<Number, Number>(new NumberAxis(), new NumberAxis());
		this.lineChart.setAnimated(false);
		this.lineChartPane.getChildren().add(this.lineChart);
		this.lineChart.setVisible(false);
		this.lineChart.getXAxis().labelProperty().set("Year");
		this.lineChart.getYAxis().labelProperty().set("Frequency");
	}

	private void setupComboboxes() {
		ObservableList<String> genderOptions = FXCollections.observableArrayList("", "MALE", "FEMALE");
		this.genderComboBox.setItems(genderOptions);
		this.lineGraphGenderComboBox.setItems(genderOptions);
	}

	private void setupBindings() {
		this.currentStateLabel.textProperty().bindBidirectional(this.viewmodel.stateProperty());
		this.namesListView.itemsProperty().bindBidirectional(this.viewmodel.namesProperty());
		this.nameField.textProperty().bindBidirectional(this.viewmodel.nameProperty());
		this.yearField.textProperty().bindBidirectional(this.viewmodel.yearProperty());
		this.genderChoice.bindBidirectional(this.viewmodel.genderProperty());
		this.frequencyField.textProperty().bindBidirectional(this.viewmodel.frequencyProperty());
		this.errorMessageField.textProperty().bindBidirectional(this.viewmodel.errorProperty());
	}
	
	private void setupBooleanBindings() {
		BooleanBinding addAndUpdateEnabled = this.nameField.textProperty().isEmpty().or(
				this.yearField.textProperty().isEmpty().or(this.genderChoice.isEmpty()).or(
						this.frequencyField.textProperty().isEmpty()));
		this.addButton.disableProperty().bind(addAndUpdateEnabled);
		this.updateFrequencyButton.disableProperty().bind(addAndUpdateEnabled);
		
		BooleanBinding deleteAndSearchEnabled = this.nameField.textProperty().isEmpty().or(
				this.yearField.textProperty().isEmpty().or(this.genderChoice.isEmpty()));
		this.deleteButton.disableProperty().bind(deleteAndSearchEnabled);
		this.searchButton.disableProperty().bind(deleteAndSearchEnabled);
		this.barChartPrevYearButton.disableProperty().bind(this.barChartYearField.textProperty().isEmpty());
		this.barGraphNextYearButton.disableProperty().bind(this.barChartYearField.textProperty().isEmpty());
		this.lineGraphSubmitButton.disableProperty().bind(this.lineGraphNameField.textProperty().isEmpty().or(
				this.lineChartGenderChoice.isEmpty()));
	}
	
	private void setupChangeListeners() {
		this.setupGenderChoiceListener();
		this.setupSelectedItemListener();
		this.setupNameFieldListener();
		this.setupYearFieldListener();
		this.setupFrequencyFieldListener();
		this.setupBarGraphYearChangeListener();
		this.setupLineGraphNameListener();
	}

	private void setupFrequencyFieldListener() {
		this.frequencyField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.matches("[1-9]?|[1-9]\\d{0,}")) {
					this.frequencyField.setText(oldValue);
					this.frequencyFormatLabel.setVisible(true);
				} else {
					this.frequencyFormatLabel.setVisible(false);
				}
			}
		});
	}

	private void setupYearFieldListener() {
		this.yearField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.matches("\\d{0,4}")) {
					this.yearField.setText(oldValue);
					this.yearFormatLabel.setVisible(true);
				} else if (!newValue.matches("\\d{4}")) {
					this.yearFormatLabel.setVisible(true);
				} else {
					this.yearFormatLabel.setVisible(false);
				}
			}
		});
	}

	private void setupNameFieldListener() {
		this.nameField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.matches("[A-Z]?|[A-Z][a-z]{0,}")) {
					this.nameField.setText(oldValue);
					this.nameFormatLabel.setVisible(true);
				} else {
					this.nameFormatLabel.setVisible(false);
				}
			}
		});
	}

	private void setupSelectedItemListener() {
		this.namesListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				this.nameField.textProperty().set(newValue.getNameKey().getName());
				this.yearField.textProperty().set("" + newValue.getNameKey().getBirthYear());
				this.genderChoice.set(newValue.getNameKey().getGender());
				this.frequencyField.textProperty().set("" + newValue.getFrequency());
			}
		});
	}

	private void setupGenderChoiceListener() {
		this.genderChoice.addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (newValue.equalsIgnoreCase("MALE")) {
					this.genderComboBox.setValue("MALE");
				} else if (newValue.equalsIgnoreCase("FEMALE")) {
					this.genderComboBox.setValue("FEMALE");
				} else {
					this.genderComboBox.setValue("");
				}
			}
		});
	}
	
	private void setupBarGraphYearChangeListener() {
		this.barChartYearField.focusedProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue) {
				this.submitBarGraphYear();
			}
		});
		this.barChartYearField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.matches("\\d{0,4}")) {
					this.barChartYearField.setText(oldValue);
				}
			}
		});
	}
	
	private void setupLineGraphNameListener() {
		this.lineGraphNameField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.matches("[A-Z]?|[A-Z][a-z]{0,}")) {
					this.lineGraphNameField.setText(oldValue);
					this.lineChartNameFormatLabel.setVisible(true);
				} else {
					this.lineChartNameFormatLabel.setVisible(false);
				}
			}
		});
	}
	
	private void setupBarGraphBindings() {
		this.barChart.dataProperty().bind(this.chartInteraction.barChartDataProperty());
		this.barChart.titleProperty().bind(this.chartInteraction.barChartTitleProperty());
		((CategoryAxis) this.barChart.getXAxis()).setCategories(this.chartInteraction.namesList());
	}
	
	private void setupLineGraphBindings() {
		this.lineChart.dataProperty().bind(this.chartInteraction.lineChartDataProperty());
		this.lineChart.titleProperty().bind(this.chartInteraction.lineChartTitleProperty());
		this.lineChartLowerBound.bind(this.chartInteraction.lineChartLowerBound());
		this.lineChartUpperBound.bind(this.chartInteraction.lineChartUpperBound());
	}
	
	@FXML
	void handleGenderComboBoxAction(ActionEvent event) {
		this.viewmodel.genderProperty().set(this.genderComboBox.getValue());
	}

	@FXML
	void handleAdd(ActionEvent event) {
		try {
			if (!this.viewmodel.addBabyName()) {
				this.errorMessageField.textProperty().set("baby name was not added");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.errorMessageField.textProperty().set("could not add because: " + ex.getMessage());
		}
	}
	
	@FXML
	void handleDelete(ActionEvent event) {
		try {
			if (!this.viewmodel.removeBabyName()) {
				this.errorMessageField.textProperty().set("baby name was not removed");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.errorMessageField.textProperty().set("couldn't remove due to: " + ex.getMessage());
		}
	}
	
	@FXML
	void handleSearch(ActionEvent event) {
		try {
			if (!this.viewmodel.searchForFrequency()) {
				this.errorMessageField.textProperty().set("No matching BabyName found");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.errorMessageField.textProperty().set("Search error: " + ex.getMessage());
		}
	}
	
	@FXML
	void handleUpdateFrequency(ActionEvent event) {
		try {
			if (!this.viewmodel.updateFrequency()) {
				this.errorMessageField.textProperty().set("Could not update frequency");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.errorMessageField.textProperty().set("Update error: " + ex.getMessage());
		}
	}
	
	@FXML
	void handleReload(ActionEvent event) {
		this.viewmodel.reloadListView();
	}
	
	@FXML
	void handleBarGraphPrevYear(ActionEvent event) {
		int currentYear = Integer.parseInt(this.barChartYearField.textProperty().get());
		if (currentYear <= 1776) {
			this.barGraphYearFormatLabel.setVisible(true);
		} else {
			this.barGraphYearFormatLabel.setVisible(false);
			this.barChartYearField.textProperty().set("" + (currentYear - 1));
			this.submitBarGraphYear();
		}
	}
	
	@FXML
    void handleBarGraphYearKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			this.submitBarGraphYear();
		}
    }
	
	private void submitBarGraphYear() {
		this.errorMessageField.textProperty().set("");
		if (this.barChartYearField.textProperty().get().matches("\\d{4}") && Integer.parseInt(this.barChartYearField.textProperty().get()) >= 1776
				&& Integer.parseInt(this.barChartYearField.textProperty().get()) <= LocalDate.now().getYear()) {
			this.barGraphYearFormatLabel.setVisible(false);
			this.setupBarGraphBindings();
			try {
				this.chartInteraction.generateBarChartData(Integer.parseInt(this.barChartYearField.textProperty().get()), this.viewmodel.babyMap());
				this.barChart.setMaxSize(this.barChartPane.getWidth(), this.barChartPane.getHeight());
				this.barChart.setVisible(true);
			} catch (IllegalArgumentException ex) {
				this.errorMessageField.textProperty().set(ex.getMessage());
				this.barChart.setVisible(false);
			}
		} else {
			this.barGraphYearFormatLabel.setVisible(true);
			this.barChart.setVisible(false);
		}
	}

	@FXML
	void handleBarGraphNextYear(ActionEvent event) {
		int currentYear = Integer.parseInt(this.barChartYearField.textProperty().get());
		if (currentYear >= LocalDate.now().getYear()) {
			this.barGraphYearFormatLabel.setVisible(true);
		} else {
			this.barGraphYearFormatLabel.setVisible(false);
			this.barChartYearField.textProperty().set("" + (currentYear + 1));
			this.submitBarGraphYear();
		}
	}
	
	@FXML
    void handleLineChartGenderComboBoxAction(ActionEvent event) {
		this.lineChartGenderChoice.set(this.lineGraphGenderComboBox.getValue());
    }

	@FXML
	void handleLineGraphSubmit(ActionEvent event) {
		this.setupLineGraphBindings();
		if (!this.chartInteraction.generateLineChartData(this.lineGraphNameField.textProperty().get(), 
				this.lineGraphGenderComboBox.getValue(), this.viewmodel.babyMap())) {
			this.errorMessageField.textProperty().set("No matching BabyNames found");
			this.lineChart.setVisible(false);
		} else {
			this.errorMessageField.textProperty().set("");
			this.lineChart.setMaxSize(this.lineChartPane.getWidth(), this.lineChartPane.getHeight());
			NumberAxis xAxis = (NumberAxis) this.lineChart.getXAxis();
			xAxis.setAutoRanging(false);
			xAxis.setLowerBound(((this.lineChartLowerBound.get() + 5) / 10) * 10);
			xAxis.setUpperBound(((this.lineChartUpperBound.get() + 5) / 10) * 10);
			xAxis.setTickUnit(10);
			xAxis.setTickLabelFormatter(new NumberStringConverter("####"));
			this.lineChart.legendVisibleProperty().set(false);
			this.lineChart.setVisible(true);
		}
		
	}
	
	@FXML
	void handleFileMenuOpenButton(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open baby name frequency file");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Name Data File *.csv or *.txt", "*.csv", "*.txt"), new ExtensionFilter("All Files", "*.*"));
		Stage stage =  (Stage) this.guiPane.getScene().getWindow();
		File selectedFile = fileChooser.showOpenDialog(stage);
		if (selectedFile != null) {
			if (!this.viewmodel.addFromFile(selectedFile)) {
				this.errorMessageField.textProperty().set("Could not import file");
			} else {
				this.errorMessageField.textProperty().set("");
			}
		}
	}

	@FXML
	void handleFileMenuSaveButton(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save baby name frequency file");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Name Data File *.csv or *.txt", "*.csv", "*.txt"), new ExtensionFilter("All Files", "*.*"));
		Stage stage =  (Stage) this.guiPane.getScene().getWindow();
		File selectedFile = fileChooser.showSaveDialog(stage);
		if (selectedFile != null) {
			if (!this.viewmodel.saveToFile(selectedFile)) {
				this.errorMessageField.textProperty().set("Could not save to file");
			} else {
				this.errorMessageField.textProperty().set("");
			}
		}
	}

	@FXML
	void handleSortMenuByNameButton(ActionEvent event) {
		this.viewmodel.sortByName();
	}
	
	@FXML
	void handleSortMenuByGenderButton(ActionEvent event) {
		this.viewmodel.sortByGender();
	}

}